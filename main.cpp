/*
 * Copyright 2012 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QtCore/QStringList>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include "teststarter.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    if(app.argc()!=2) {
        qDebug() << "Usage: " << qPrintable(app.arguments().first()) << "<yourtest.qml ...>";
        return 1;
    }
    QStringList args = app.arguments();
    args.removeFirst();
    
    TestStarter foo;
    foreach(const QString& arg, args) {
        QUrl target(arg);
        QDir d(target.toLocalFile());
        if(d.exists()) {
            QStringList files = d.entryList(QStringList("*.qml"), QDir::NoFilter);
            foreach(const QString& file, files) {
                foo.addTest(QUrl(d.absoluteFilePath(file)));
            }
        } else {
            foo.addTest(target);
        }
    }
    return app.exec();
}
