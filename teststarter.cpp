/*
 * Copyright 2012 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "teststarter.h"
#include "quicktest.h"
#include <iostream>
#include <qcoreapplication.h>

TestStarter::TestStarter(QObject* parent)
    : QObject(parent)
    , m_current(0)
    , m_allCorrect(true)
{
}

void TestStarter::addTest(const QUrl& target)
{
    if(m_urls.isEmpty())
        QMetaObject::invokeMethod(this, "start", Qt::QueuedConnection);
    m_urls += target;
}

void TestStarter::start()
{
    QUrl url = m_urls.takeFirst();
    m_current->deleteLater();
    m_current = new QuickTest(this);
    connect(m_current, SIGNAL(finished(QString)), SLOT(testFinished(QString)));
    m_current->setUrl(url);
}

static QString filename(const QUrl& url)
{
    QString path = url.path();
    int idx = path.lastIndexOf('/')+1;
    return path.mid(idx);
}

void TestStarter::testFinished(const QString& message)
{
    QString msg = message;
    if(msg.endsWith('\n'))
        msg.chop(1);
    if(msg.isEmpty())
        std::cout << "** test finished correctly (" << qPrintable(filename(m_current->url())) << ") **\n";
    else
        std::cout << "!! test failed ("<< qPrintable(filename(m_current->url())) << ") !! because:" << qPrintable('\n'+msg+'\n');
    m_allCorrect &= msg.isEmpty();
    
    if(m_urls.isEmpty())
        qApp->exit(!m_allCorrect);
    else
        start();
}
