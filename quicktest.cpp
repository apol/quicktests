/*
 * Copyright 2012 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "quicktest.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>
#include <QDebug>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeComponent>

#ifdef KDE
#include <kdeclarative.h>
#endif

QuickTest::QuickTest(QObject* parent)
    : QObject(parent)
    , m_object(0)
    , m_done(false)
    , m_engine(new QDeclarativeEngine(this))
    , m_runTest(0)
{
#ifdef KDE
    KDeclarative kdeclarative;
    kdeclarative.setDeclarativeEngine(m_engine);
    kdeclarative.initialize();
    kdeclarative.setupBindings();
#endif
}

QuickTest::~QuickTest()
{}

void QuickTest::setUrl(const QUrl& testUrl)
{
    m_context = new QDeclarativeComponent(m_engine, this);
    connect(m_context, SIGNAL(statusChanged(QDeclarativeComponent::Status)), SLOT(statusChanged(QDeclarativeComponent::Status)));
    m_context->loadUrl(testUrl);
}

QUrl QuickTest::url() const
{
    return m_context->url();
}

void QuickTest::finishWithError(const QString& message)
{
    m_done = true;
    emit finished(message);
}

void QuickTest::statusChanged(QDeclarativeComponent::Status s)
{
    switch(s) {
        case QDeclarativeComponent::Error:
            finishWithError(m_context->errorString());
            break;
        case QDeclarativeComponent::Loading:
        case QDeclarativeComponent::Null:
            break;
        case QDeclarativeComponent::Ready:
            initializeObject(m_context->create());
            break;
    }
}

void QuickTest::initializeObject(QObject* obj)
{
    m_object = obj;

    if(obj) {
        const QMetaObject* metaObject = obj->metaObject();
        if(metaObject->indexOfProperty("finished")<0) {
            finishWithError("finished property missing");
        }
        if(metaObject->indexOfProperty("error")<0) {
            finishWithError("error property missing");
        }
        connect(obj, SIGNAL(errorChanged()), SLOT(errorChanged()));
        connect(obj, SIGNAL(finishedChanged()), SLOT(finishedChanged()));

        m_runTest = metaObject->methodOffset();
        errorChanged();
        finishedChanged();
    }
}

void QuickTest::finishedChanged()
{
    Q_ASSERT(m_object);
    const QMetaObject* metaObject = m_object->metaObject();
    for (; m_runTest < metaObject->methodCount() ; m_runTest++) {
        QMetaMethod method = metaObject->method(m_runTest);
        if(QByteArray(method.signature()).startsWith("test")) {
            qDebug() << "running test" << method.signature();
            method.invoke(m_object);
            if(!m_object->property("finished").toBool()) {
                return;
            }
        }
    }
    
    if(!m_done && m_object->property("finished").toBool() && m_object->property("errors").toString().isEmpty()) {
        m_done = true;
        emit finished(QString());
    }
}

void QuickTest::errorChanged()
{
    QString error = m_object->property("error").toString();
    m_done = !error.isEmpty();
    if(m_done)
        finishWithError(error);
}

bool QuickTest::isDone() const
{
    return m_done;
}
