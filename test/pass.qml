import QtQuick 1.0

QtObject {
    property bool finished: true
    property string error: ""
}
