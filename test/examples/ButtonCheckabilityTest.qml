import QtQuick 1.1
import org.kde.plasma.components 0.1
import QuickTests 1.0

QuickTest
{
    finished: true
    error: button.checked ? "checked an uncheckable button" : ""
    
    Button {
        id: button
        checkable: false
        checked: true
    }
}
