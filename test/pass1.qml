import QtQuick 1.1

Item {
    id: test
    property bool finished: false
    property string error: ""
    
    Timer {
        interval: 500
        running: true
        onTriggered: { test.finished = true }
    }
}
