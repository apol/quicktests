import QtQuick 1.0
import QuickTests 1.0

QuickTest {
    finished: false
    error: ""

    Timer {
        id: timer
        interval: 200
        onTriggered: {
            error = "timeout";
            finished=true
        }
    }

    function test1() {
        timer.running=true
    }
}
