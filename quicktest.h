/*
 * Copyright 2012 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QUICKTESTS_H
#define QUICKTESTS_H

#include <QtCore/QObject>
#include <QtDeclarative/QDeclarativeComponent>

class QuickTest : public QObject
{
    Q_OBJECT
    public:
        QuickTest(QObject* parent = 0);
        virtual ~QuickTest();

        bool isDone() const;
        void setUrl(const QUrl& url);
        QUrl url() const;

    signals:
        void finished(const QString& error);

    private slots:
        void errorChanged();
        void statusChanged(QDeclarativeComponent::Status s);
        void finishedChanged();

    private:
        void finishWithError(const QString& message);
        void initializeObject(QObject* create);

        QObject* m_object;
        QDeclarativeComponent* m_context;
        bool m_done;
        QDeclarativeEngine* m_engine;
        int m_runTest;
};

#endif
