/*
 * Copyright 2012 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTSTARTER_H
#define TESTSTARTER_H

#include <QObject>
#include <QUrl>

class QuickTest;
class TestStarter : public QObject
{
    Q_OBJECT
    public:
        explicit TestStarter(QObject* parent = 0);
        void addTest(const QUrl& target);

    private slots:
        void start();
        void testFinished(const QString& message);

    private:
        QList<QUrl> m_urls;
        QuickTest* m_current;
        bool m_allCorrect;
};

#endif
